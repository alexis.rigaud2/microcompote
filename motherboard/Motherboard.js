var mongoose = require('mongoose');  
var MotherboardSchema = new mongoose.Schema({  
  name: String,
  socket: {type: String, enum: ['AM4', 'SP3', 'TR4', 'LGA 2066', 'LGA 3647', 'LGA 1151', 'FM2+', 'AM1', 'LGA 1150', 'LGA 1356', 'FM2', 'LGA 2011', 'LGA 1155', 'FM1', 'FS1', 'AM3+', 'G34', 'C32', 'LGA 1248', 'LGA 1567', 'AM3', 'LGA 1156', 'LGA 1366', 'G1']},
  memslots: {type: Number, enum: ['1', '2', '3', '4', '5', '6', '7', '8']},
  memfrequency: Number,
  format: {type: String, enum: ['Pico-ITX', 'Nano-ITX', 'Mini-ATX', 'Micro-ATX', 'Standard-ATX']}
});
mongoose.model('Motherboard', MotherboardSchema);

module.exports = mongoose.model('Motherboard');