var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var VerifyToken = require('../auth/VerifyToken');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
var Motherboard = require('./Motherboard');

// Create a new motherboard (must be auth)
router.post('/', VerifyToken, function (req, res) {
    Motherboard.create({
            name : req.body.name,
            chipset : req.body.chipset,
            socket : req.body.socket,
            memslots : req.body.memslots,
            memfrequency : req.body.memfrequency,
            format : req.body.format
        }, 
        function (err, motherboard) {
            if (err) return res.status(500).send("There was a problem adding the information to the database.");
            res.status(200).send(motherboard);
        });
});

// List all motherboards
router.get('/', function (req, res) {
    Motherboard.find({}, function (err, motherboards) {
        if (err) return res.status(500).send("There was a problem finding the motherboards.");
        res.status(200).send(motherboards);
    });
});

// Show a specific motherboard
router.get('/:id', function (req, res) {
    Motherboard.findById(req.params.id, function (err, motherboard) {
        if (err) return res.status(500).send("There was a problem finding the motherboard.");
        if (!motherboard) return res.status(404).send("No motherboard found.");
        res.status(200).send(motherboard);
    });
});

// Delete a motherboard (must be auth)
router.delete('/:id', VerifyToken, function (req, res) {
    Motherboard.findByIdAndRemove(req.params.id, function (err, motherboard) {
        if (err) return res.status(500).send("There was a problem deleting the motherboard.");
        res.status(200).send("Motherboard was deleted.");
    });
});

// Modify a motherboard (must be auth)
router.put('/:id', VerifyToken, function (req, res) {
    Motherboard.findByIdAndUpdate(req.params.id, req.body, {new: true}, function (err, motherboard) {
        if (err) return res.status(500).send("There was a problem updating the motherboard.");
        res.status(200).send(motherboard);
    });
});


module.exports = router;