var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var VerifyToken = require('../auth/VerifyToken');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
var Disk = require('./Disk');

// Create a new disk (must be auth)
router.post('/', VerifyToken, function (req, res) {
    Disk.create({
            name : req.body.name,
            type : req.body.type,
            format : req.body.format,
            capacity : req.body.capacity
        }, 
        function (err, disk) {
            if (err) return res.status(500).send("There was a problem adding the information to the database.");
            res.status(200).send(disk);
        });
});

// List all disks
router.get('/', function (req, res) {
    Disk.find({}, function (err, disks) {
        if (err) return res.status(500).send("There was a problem finding the disks.");
        res.status(200).send(disks);
    });
});

// Show a specific disk
router.get('/:id', function (req, res) {
    Disk.findById(req.params.id, function (err, disk) {
        if (err) return res.status(500).send("There was a problem finding the disk.");
        if (!disk) return res.status(404).send("No disk found.");
        res.status(200).send(disk);
    });
});

// Delete a disk (must be auth)
router.delete('/:id', VerifyToken, function (req, res) {
    Disk.findByIdAndRemove(req.params.id, function (err, disk) {
        if (err) return res.status(500).send("There was a problem deleting the disk.");
        res.status(200).send("Disk was deleted.");
    });
});

// Modify a disk (must be auth)
router.put('/:id', VerifyToken, function (req, res) {
    Disk.findByIdAndUpdate(req.params.id, req.body, {new: true}, function (err, disk) {
        if (err) return res.status(500).send("There was a problem updating the disk.");
        res.status(200).send(disk);
    });
});


module.exports = router;