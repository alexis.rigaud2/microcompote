var mongoose = require('mongoose');  
var DiskSchema = new mongoose.Schema({  
  name: String,
  type: {type: String, enum: ['HDD', 'SSD', 'Hybrid']},
  format: {type: String, enum: ['2.5"', '3.5"', 'M2']},
  capacity: Number
});
mongoose.model('Disk', DiskSchema);

module.exports = mongoose.model('Disk');