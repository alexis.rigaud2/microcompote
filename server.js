var app = require('./app');
var cors = require('cors')
var port = process.env.PORT || 3000;

// Call of CORS to accept cross origin
app.use(cors());

// Declaration of the listening port (3000)
var server = app.listen(port, function() {
  console.log('Microcompote server is running on port ' + port);
});