var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var VerifyToken = require('../auth/VerifyToken');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
var Case = require('./Case');

// Create a new case (must be auth)
router.post('/', VerifyToken, function (req, res) {
    Case.create({
            name : req.body.name,
            size : req.body.size,
            format : req.body.format,
            weight : req.body.weight
        }, 
        function (err, case) {
            if (err) return res.status(500).send("There was a problem adding the information to the database.");
            res.status(200).send(case);
        });
});

// List all cases
router.get('/', function (req, res) {
    Case.find({}, function (err, cases) {
        if (err) return res.status(500).send("There was a problem finding the cases.");
        res.status(200).send(cases);
    });
});

// Show a specific case
router.get('/:id', function (req, res) {
    Case.findById(req.params.id, function (err, case) {
        if (err) return res.status(500).send("There was a problem finding the case.");
        if (!case) return res.status(404).send("No case found.");
        res.status(200).send(case);
    });
});

// Delete a case (must be auth)
router.delete('/:id', VerifyToken, function (req, res) {
    Case.findByIdAndRemove(req.params.id, function (err, case) {
        if (err) return res.status(500).send("There was a problem deleting the case.");
        res.status(200).send("Case was deleted.");
    });
});

// Modify a case (must be auth)
router.put('/:id', VerifyToken, function (req, res) {
    Case.findByIdAndUpdate(req.params.id, req.body, {new: true}, function (err, case) {
        if (err) return res.status(500).send("There was a problem updating the case.");
        res.status(200).send(case);
    });
});


module.exports = router;