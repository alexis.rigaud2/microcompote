var mongoose = require('mongoose');  
var CaseSchema = new mongoose.Schema({  
  name: String,
  size: {type: String, enum: ['Tiny', 'Small', 'Mid', 'Large', 'X-Large']},
  format: {type: String, enum: ['Pico-ITX', 'Nano-ITX', 'Mini-ATX', 'Micro-ATX', 'Standard-ATX']},
  weight: Number
});
mongoose.model('Case', CaseSchema);

module.exports = mongoose.model('Case');