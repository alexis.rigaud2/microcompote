var jwt = require('jsonwebtoken');
var config = require('../config');

function verifyToken(req, res, next) {

  // Check for x-access-token parameter to token
  var token = req.headers['x-access-token'];
  if (!token) 
    return res.status(403).send({ auth: false, message: 'No token provided.' });

  // Verifies secret key (should be an environment variable)
  jwt.verify(token, config.secret, function(err, decoded) {      
    if (err) 
      return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });

    // Save token
    req.userId = decoded.id;
    next();
  });

}

module.exports = verifyToken;