var express = require('express');
var app = express();
var db = require('./db');

// Route /test
app.get('/test', function (req, res) {
  res.status(200).send('The Microcompote API is working !');
});

// Route /auth for authentification
var AuthController = require('./auth/AuthController');
app.use('/auth', AuthController);

// Route /users
var UserController = require('./user/UserController');
app.use('/users', UserController);

// Route /processors
var ProcessorController = require('./processor/ProcessorController');
app.use('/processors', ProcessorController);

// Route /motherboards
var MotherboardController = require('./motherboard/MotherboardController');
app.use('/motherboards', MotherboardController);

// Route /memorys
var MemoryController = require('./memory/MemoryController');
app.use('/memorys', MemoryController);

// Route /graphics
var GraphicController = require('./graphic/GraphicController');
app.use('/graphics', GraphicController);

// Route /disks
var DiskController = require('./disk/DiskController');
app.use('/disks', DiskController);

// Route /cases
var CaseController = require('./case/CaseController');
app.use('/cases', CaseController);

// Route /alimentations
var AlimentationController = require('./alimentation/AlimentationController');
app.use('/alimentations', AlimentationController);

module.exports = app;