var mongoose = require('mongoose');  
var ProcessorSchema = new mongoose.Schema({  
  name: String,
  socket: {type: String, enum: ['AM4', 'SP3', 'TR4', 'LGA 2066', 'LGA 3647', 'LGA 1151', 'FM2+', 'AM1', 'LGA 1150', 'LGA 1356', 'FM2', 'LGA 2011', 'LGA 1155', 'FM1', 'FS1', 'AM3+', 'G34', 'C32', 'LGA 1248', 'LGA 1567', 'AM3', 'LGA 1156', 'LGA 1366', 'G1']},
  frequency: Number,
  turbofrequency: Number,
  cores: Number,
  threads: Number,
  cache: Number,
  tdp: Number
});
mongoose.model('Processor', ProcessorSchema);

module.exports = mongoose.model('Processor');