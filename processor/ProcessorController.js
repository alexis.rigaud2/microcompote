var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var VerifyToken = require('../auth/VerifyToken');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
var Processor = require('./Processor');

// Create a new processor (must be auth)
router.post('/', VerifyToken, function (req, res) {
    Processor.create({
            name : req.body.name,
            frequency : req.body.frequency,
            turbofrequency : req.body.turbofrequency,
            cores : req.body.cores,
            threads : req.body.threads,
            cache : req.body.cache,
            tdp : req.body.tdp
        }, 
        function (err, processor) {
            if (err) return res.status(500).send("There was a problem adding the information to the database.");
            res.status(200).send(processor);
        });
});

// List all processors
router.get('/', function (req, res) {
    Processor.find({}, function (err, processors) {
        if (err) return res.status(500).send("There was a problem finding the processors.");
        res.status(200).send(processors);
    });
});

// Show a specific processor
router.get('/:id', function (req, res) {
    Processor.findById(req.params.id, function (err, processor) {
        if (err) return res.status(500).send("There was a problem finding the processor.");
        if (!processor) return res.status(404).send("No processor found.");
        res.status(200).send(processor);
    });
});

// Delete a processor (must be auth)
router.delete('/:id', VerifyToken, function (req, res) {
    Processor.findByIdAndRemove(req.params.id, function (err, processor) {
        if (err) return res.status(500).send("There was a problem deleting the processor.");
        res.status(200).send("Processor was deleted.");
    });
});

// Modify a processor (must be auth)
router.put('/:id', VerifyToken, function (req, res) {
    Processor.findByIdAndUpdate(req.params.id, req.body, {new: true}, function (err, processor) {
        if (err) return res.status(500).send("There was a problem updating the processor.");
        res.status(200).send(processor);
    });
});


module.exports = router;