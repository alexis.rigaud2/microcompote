var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var VerifyToken = require('../auth/VerifyToken');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
var Graphic = require('./Graphic');

// Create a new graphic (must be auth)
router.post('/', VerifyToken, function (req, res) {
    Graphic.create({
            name : req.body.name,
            memory : req.body.memory,
            frequency : req.body.frequency,
            tdp : req.body.tdp
        }, 
        function (err, graphic) {
            if (err) return res.status(500).send("There was a problem adding the information to the database.");
            res.status(200).send(graphic);
        });
});

// List all graphics
router.get('/', function (req, res) {
    Graphic.find({}, function (err, graphics) {
        if (err) return res.status(500).send("There was a problem finding the graphics.");
        res.status(200).send(graphics);
    });
});

// Show a specific graphic
router.get('/:id', function (req, res) {
    Graphic.findById(req.params.id, function (err, graphic) {
        if (err) return res.status(500).send("There was a problem finding the graphic.");
        if (!graphic) return res.status(404).send("No graphic found.");
        res.status(200).send(graphic);
    });
});

// Delete a graphic (must be auth)
router.delete('/:id', VerifyToken, function (req, res) {
    Graphic.findByIdAndRemove(req.params.id, function (err, graphic) {
        if (err) return res.status(500).send("There was a problem deleting the graphic.");
        res.status(200).send("Graphic was deleted.");
    });
});

// Modify a graphic (must be auth)
router.put('/:id', VerifyToken, function (req, res) {
    Graphic.findByIdAndUpdate(req.params.id, req.body, {new: true}, function (err, graphic) {
        if (err) return res.status(500).send("There was a problem updating the graphic.");
        res.status(200).send(graphic);
    });
});


module.exports = router;