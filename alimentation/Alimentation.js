var mongoose = require('mongoose');  
var AlimentationSchema = new mongoose.Schema({  
  name: String,
  power: Number,
  yield: {type: String, enum: ['Bronze', 'Silver', 'Gold', 'Platinum', 'Titanium']},
  modular: Boolean
});
mongoose.model('Alimentation', AlimentationSchema);

module.exports = mongoose.model('Alimentation');