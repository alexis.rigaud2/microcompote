var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var VerifyToken = require('../auth/VerifyToken');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
var Alimentation = require('./Alimentation');

// Create a new alimentation (must be auth)
router.post('/', VerifyToken, function (req, res) {
    Alimentation.create({
            name : req.body.name,
            power : req.body.power,
            yield : req.body.yield,
            modular : req.body.modular
        }, 
        function (err, alimentation) {
            if (err) return res.status(500).send("There was a problem adding the information to the database.");
            res.status(200).send(alimentation);
        });
});

// List all alimentations
router.get('/', function (req, res) {
    Alimentation.find({}, function (err, alimentations) {
        if (err) return res.status(500).send("There was a problem finding the alimentations.");
        res.status(200).send(alimentations);
    });
});

// Show a specific alimentation
router.get('/:id', function (req, res) {
    Alimentation.findById(req.params.id, function (err, alimentation) {
        if (err) return res.status(500).send("There was a problem finding the alimentation.");
        if (!alimentation) return res.status(404).send("No alimentation found.");
        res.status(200).send(alimentation);
    });
});

// Delete a alimentation (must be auth)
router.delete('/:id', VerifyToken, function (req, res) {
    Alimentation.findByIdAndRemove(req.params.id, function (err, alimentation) {
        if (err) return res.status(500).send("There was a problem deleting the alimentation.");
        res.status(200).send("Alimentation was deleted.");
    });
});

// Modify a alimentation (must be auth)
router.put('/:id', VerifyToken, function (req, res) {
    Alimentation.findByIdAndUpdate(req.params.id, req.body, {new: true}, function (err, alimentation) {
        if (err) return res.status(500).send("There was a problem updating the alimentation.");
        res.status(200).send(alimentation);
    });
});


module.exports = router;