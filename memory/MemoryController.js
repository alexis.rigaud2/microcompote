var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var VerifyToken = require('../auth/VerifyToken');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
var Memory = require('./Memory');

// Create a new memory (must be auth)
router.post('/', VerifyToken, function (req, res) {
    Memory.create({
            name : req.body.name,
            type : req.body.type,
            capacity : req.body.capacity,
            frequency : req.body.frequency,
            cas : req.body.cas
        }, 
        function (err, memory) {
            if (err) return res.status(500).send("There was a problem adding the information to the database.");
            res.status(200).send(memory);
        });
});

// List all memorys
router.get('/', function (req, res) {
    Memory.find({}, function (err, memorys) {
        if (err) return res.status(500).send("There was a problem finding the memorys.");
        res.status(200).send(memorys);
    });
});

// Show a specific memory
router.get('/:id', function (req, res) {
    Memory.findById(req.params.id, function (err, memory) {
        if (err) return res.status(500).send("There was a problem finding the memory.");
        if (!memory) return res.status(404).send("No memory found.");
        res.status(200).send(memory);
    });
});

// Delete a memory (must be auth)
router.delete('/:id', VerifyToken, function (req, res) {
    Memory.findByIdAndRemove(req.params.id, function (err, memory) {
        if (err) return res.status(500).send("There was a problem deleting the memory.");
        res.status(200).send("Memory was deleted.");
    });
});

// Modify a memory (must be auth)
router.put('/:id', VerifyToken, function (req, res) {
    Memory.findByIdAndUpdate(req.params.id, req.body, {new: true}, function (err, memory) {
        if (err) return res.status(500).send("There was a problem updating the memory.");
        res.status(200).send(memory);
    });
});


module.exports = router;