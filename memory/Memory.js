var mongoose = require('mongoose');  
var MemorySchema = new mongoose.Schema({  
  name: String,
  type: {type: String, enum: ['DDR', 'DDR2', 'DDR3', 'DDR4']},
  capacity: {type: Number, enum: ['1', '2', '4', '6', '8', '10', '12', '16', '32', '64', '128']},
  frequency: Number,
  cas: {type: Number, enum: ['CAS 1', 'CAS 2', 'CAS 3', 'CAS 4', 'CAS 5', 'CAS 6', 'CAS 7', 'CAS 8', 'CAS 9', 'CAS 10', 'CAS 11', 'CAS 12', 'CAS 13', 'CAS 14', 'CAS 15', 'CAS 16', 'CAS 17', 'CAS 18', 'CAS 19']}
});
mongoose.model('Memory', MemorySchema);

module.exports = mongoose.model('Memory');