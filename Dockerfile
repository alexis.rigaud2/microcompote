FROM centos:centos7.6.1810

RUN yum -y update; yum clean all
RUN yum -y install epel-release; yum clean all
RUN yum -y install nodejs npm; yum clean all

WORKDIR /app
COPY package.json /app
RUN npm install
COPY . /app
EXPOSE 3000

CMD [ "node", "server.js" ]